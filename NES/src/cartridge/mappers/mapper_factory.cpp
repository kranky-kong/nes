#include <cartridge\mappers\mapper.h>
#include <cartridge\mappers\mapper_factory.h>
#include <cartridge\mappers\mapper_000.h>
#include <cartridge\mappers\mapper_001.h>
#include <exception>

Mapper MapperFactory::Create(int mapperNumber) {
  switch (mapperNumber) {
    case 0x000: return Mapper000();
    case 0x001: return Mapper001();
    default: throw std::exception("Mapper not implemented");
  }
}
