#include <console\cpu_memory_map.h>

CpuMemoryMap::CpuMemoryMap(Cartridge &cart, BYTE * ram) {
  this->cartridge = &cart;
  this->ram = ram;
}

BYTE CpuMemoryMap::read(WORD address) {
  address = translateAddress(address);
  if (address < 0x800) {
    return ram[address];
  } else if (address < 0x2008) {
    return 0;
  } else if (address < 0x4018) {
    return 0;
  } else if (address < 0x4020) {
    return 0;
  } else {
    return cartridge->read(address);
  }
}

void CpuMemoryMap::write(WORD address, BYTE value) {
  address = translateAddress(address);
  if (address < 0x800) {
    ram[address] = value;
  } else if (address < 0x2008) {

  } else if (address < 0x4018) {

  } else if (address < 0x4020) {

  } else {
    cartridge->write(address, value);
  }
}

WORD CpuMemoryMap::translateAddress(WORD address) {
  if (address < 0x2000) {
    return address % 0x800;
  }
  if (address < 0x4000) {
    return (address % 0x0008) + 0x2000;
  }
  return address;
}
