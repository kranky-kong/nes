#pragma once

#include <cartridge\mappers\mapper.h>

class Mapper000 : public Mapper {
public:
  BYTE read(WORD address, Cartridge* cartridge) override;
  void write(WORD address, BYTE value, Cartridge* cartridge) override;
};