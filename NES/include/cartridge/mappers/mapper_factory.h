#pragma once

#include <cartridge\mappers\mapper.h>

class MapperFactory {
public:
  static Mapper Create(int mapperNumber);

private:
  MapperFactory();
};