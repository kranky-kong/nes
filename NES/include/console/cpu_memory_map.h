#pragma once

#include <common\globals.h>
#include <cartridge\cartridge.h>

class CpuMemoryMap {
public:
  CpuMemoryMap(Cartridge & cartridge, BYTE * ram);

  BYTE read(WORD address);
  void write(WORD address, BYTE value);

private:
  Cartridge * cartridge;
  BYTE * ram;

  WORD translateAddress(WORD address);
};
