#pragma once
#include <common\globals.h>
#include <console\cpu.h>
#include <string>

class CpuLogger {
public:
  static WORD programCounter;
  static BYTE opcode;
  static BYTE operand1;
  static BYTE operand2;
  static std::string disassembly;

  static void log(Cpu * cpu) {
    printf("%04X  %02X ", programCounter, opcode);

    if (operand1 != -1 && operand2 != -1)
      printf("%02X %02X  ", operand1, operand2);
    else if (operand1 != -1)
      printf("%02X       ", operand1);
    else
      printf("           ");

    printf("%-32s", disassembly);
    printf("A:%02X X:%02X Y:%02X P:%02X SP:%02X", cpu->regA, cpu->regX, cpu->regY, cpu->regP, cpu->regPC);

    initialize();
  }

  static void initialize() {
    programCounter = -1;
    opcode = -1;
    operand1 = -1;
    operand2 = -1;
    disassembly = "";
  }
};